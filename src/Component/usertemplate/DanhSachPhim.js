import React, { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import Modal from "react-modal";
import "./danhsachphim.scss";
import { giaoTiepAPI } from "../../redux/giaoTiepAPI";
import PageLoading from "./PageLoading";
const DanhSachPhim = () => {
  const [danhSachPhim, setDanhSachPhim] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  useEffect(() => {
    giaoTiepAPI
      .layDanhSachPhim()
      .then((result) => {
        setIsLoading(false);
        console.log(result.data.content);
        setDanhSachPhim(result.data.content);
        setIsLoading(false);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);
  const chiTietPhim = (maPhim) => {};
  const [showPopup, setShowPopup] = useState(false);
  const [trailer, setTrailer] = useState("");
  const showTrailer = (trailer) => {
    setShowPopup(true);
    setTrailer(trailer);
  };
  const closeTrailer = () => {
    setShowPopup(false);
  };
  return (
    <div id="danhSachPhim">
      <div>{isLoading ? <PageLoading /> : <></>}</div>
      <div>
        <Modal
          isOpen={showPopup}
          onRequestClose={closeTrailer}
          contentLabel="Video Popup"
          shouldCloseOnOverlayClick={true}
          overlayClassName="custom-overlay"
        >
          <button id="closeTrailer" onClick={closeTrailer}>
            <i className="fa-solid fa-xmark"></i>
          </button>
          <iframe
            width="100%"
            height="100%"
            src={trailer}
            title="YouTube video player"
            allowFullScreen
          />
        </Modal>
      </div>
      <div className="danhSachPhimContent">
        {danhSachPhim.map((item, index) => {
          return (
            <div key={index} className="phimItem">
              <div className="imgContent">
                <img src={item.hinhAnh} alt="" />
                <h1>C18</h1>
                <div className="imgOverlay">
                  <i
                    className="fa-regular fa-circle-play"
                    onClick={() => showTrailer(item.trailer)}
                  ></i>
                </div>
              </div>
              <div className="phimTitle">
                <h1>{item.tenPhim.toUpperCase()}</h1>
                <div className="moTa">
                  <p className="line-clamp-2">{item.moTa}</p>
                  <div
                    className="datVe"
                    onClick={() => chiTietPhim(item.maPhim)}
                  >
                    <NavLink to={`/chitietphim/${item.maPhim}`}>
                      Chi tiết
                    </NavLink>
                  </div>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};
export default DanhSachPhim;
