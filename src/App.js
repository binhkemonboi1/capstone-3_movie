import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";
import HomePage from "./Component/usertemplate/HomePage";
import DangNhap from "./Component/usertemplate/DangNhap";
import Page404 from "./Component/usertemplate/Page404";
import DatVeXemPhim from "./Component/usertemplate/DatVeXemPhim";
import ChiTietPhim from "./Component/usertemplate/ChiTietPhim";
import QuanLy from "./Component/admintemplate/QuanLy";
import Film from "./Component/admintemplate/Film";
import User from "./Component/admintemplate/User";
import ShowTime from "./Component/admintemplate/ShowTime";
import DetailPage from "./Component/DetailPage/DetailPage";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="/dangnhap" element={<DangNhap />} />
        <Route path="*" element={<Page404 />} />
        <Route path="/detail/:id" element={<DetailPage />} />
        <Route path="chitietphim/:id" element={<ChiTietPhim />} />
        <Route path="datvexemphim/:id" element={<DatVeXemPhim />} />
        <Route path="/quanly" element={<QuanLy />}>
          <Route path="/quanly/user" element={<User />} />
          <Route path="/quanly/film" element={<Film />} />
          <Route path="/quanly/showtime" element={<ShowTime />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
